package com.sky.parentalcontrol.service.impl;

import com.sky.movie.Certificate;
import com.sky.movie.exceptions.CertificateNotFoundException;
import com.sky.movie.exceptions.TechnicalFailureException;
import com.sky.movie.exceptions.TitleNotFoundException;
import com.sky.movie.service.MovieService;
import com.sky.parentalcontrol.service.ParentalControlService;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * The Parental Control Service Implementation.
 *
 * @author Roger Chick
 */
public class ParentalControlServiceImpl implements ParentalControlService {

    @Autowired
    private final MovieService movieService;

    public ParentalControlServiceImpl(MovieService movieService) {
        this.movieService = movieService;
    }

    /**
     * isMovieWithinParentalControlLevel takes the supplied parentalControlLevel and movieId as Strings, looks up the
     * movie certificate and compares that to the parentalControlLevel supplied.
     *
     * @param parentalControlLevel the parent control level
     * @param movieId              the unique id for a movie
     * @return It will return true if the movie is equal or below the parentalControlLevel.
     * @throws TitleNotFoundException       thrown if movie not found.
     * @throws TechnicalFailureException    thrown if a technical failure has occurred whilst calling the ParentalControlService
     * @throws CertificateNotFoundException thrown if the supplied or retrieved certificate is invalid or unknown.
     */
    @Override
    public boolean isMovieWithinParentalControlLevel(String parentalControlLevel, String movieId) throws
            TitleNotFoundException, TechnicalFailureException, CertificateNotFoundException {

        String movieCertificate = movieService.getParentalControlLevel(movieId);

        return Certificate.findByCertificateText(movieCertificate).ordinal() <= Certificate.findByCertificateText(parentalControlLevel).ordinal();
    }

}
