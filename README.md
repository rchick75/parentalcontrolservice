# Parental Control Service #

### Scenario

Sky is developing a next generation Video on Demand platform. You are
part of a software engineering team, developing services for the platform
and working on the story below.

### Prevent access to movies based on parental control level:

As a customer I don’t want my account to be able to access movies that
have a higher parental control level than my current preference setting.

Your team has partnered with the Movie Meta Data team that provides a
service that can return parental control information for a given movie.