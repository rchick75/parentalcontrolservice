package com.sky.movie.service;

import com.sky.movie.exceptions.TechnicalFailureException;
import com.sky.movie.exceptions.TitleNotFoundException;
import org.springframework.stereotype.Service;

/**
 * The Movie Service Interface.
 *
 * @author Roger Chick
 */
@Service
public interface MovieService {

    /**
     * getParentalControlLevel accepts the movie id as an input and returns the certificate for that movie as
     * a string.
     *
     * @param movieId the unique id for a movie
     * @return the certificate for that movie as a string.
     * @throws TitleNotFoundException    thrown if movie not found.
     * @throws TechnicalFailureException thrown if a technical failure has occurred whilst calling the MovieService
     */
    String getParentalControlLevel(String movieId) throws TitleNotFoundException, TechnicalFailureException;
}
