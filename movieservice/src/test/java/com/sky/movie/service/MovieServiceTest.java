package com.sky.movie.service;

import com.sky.movie.exceptions.TechnicalFailureException;
import com.sky.movie.exceptions.TitleNotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MovieServiceTest {

    @Mock
    private MovieService mockMovieService;

    @Test
    public void getParentalControlLevelPG() throws Exception {
        String theGooniesCertificate = "PG";
        String theGooniesMovieId = "tt0089218";

        when(mockMovieService.getParentalControlLevel(theGooniesMovieId)).thenReturn(theGooniesCertificate);

        String actual = mockMovieService.getParentalControlLevel(theGooniesMovieId);

        assertThat(actual, is(equalTo(theGooniesCertificate)));
    }

    @Test(expected = TitleNotFoundException.class)
    public void getParentalControlLevelThrowsTitleNotFoundException() throws Exception {
        when(mockMovieService.getParentalControlLevel("")).thenThrow(new TitleNotFoundException());
        mockMovieService.getParentalControlLevel("");
    }

    @Test(expected = TechnicalFailureException.class)
    public void getParentalControlLevel() throws Exception {
        when(mockMovieService.getParentalControlLevel(null)).thenThrow(new TechnicalFailureException());
        mockMovieService.getParentalControlLevel(null);
    }
}