package com.sky.parentalcontrol.service.impl;

import com.sky.movie.exceptions.CertificateNotFoundException;
import com.sky.movie.exceptions.TechnicalFailureException;
import com.sky.movie.exceptions.TitleNotFoundException;
import com.sky.movie.service.MovieService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ParentalControlServiceImplTest {

    @Mock
    private MovieService mockMovieService;

    @InjectMocks
    private ParentalControlServiceImpl parentalControlService;

    @Test
    public void isMovieOfPGWithinParentalControlLevelOfFifteen() throws Exception {
        String theGooniesCertificate = "PG";
        String theGooniesMovieId = "tt0089218";
        String parentalControlLevel = "15";

        when(mockMovieService.getParentalControlLevel(theGooniesMovieId)).thenReturn(theGooniesCertificate);

        assertTrue(parentalControlService.isMovieWithinParentalControlLevel(parentalControlLevel, theGooniesMovieId));
    }

    @Test
    public void isMovieOfPGWithinParentalControlLevelOfPG() throws Exception {
        String theGooniesCertificate = "PG";
        String theGooniesMovieId = "tt0089218";
        String parentalControlLevel = "PG";

        when(mockMovieService.getParentalControlLevel(theGooniesMovieId)).thenReturn(theGooniesCertificate);

        assertTrue(parentalControlService.isMovieWithinParentalControlLevel(parentalControlLevel, theGooniesMovieId));
    }

    @Test
    public void isMovieOfEighteenIsNotWithinParentalControlLevelOfU() throws Exception {
        String draculaCertificate = "18";
        String draculaMovieId = "tt0103874";
        String parentalControlLevel = "U";

        when(mockMovieService.getParentalControlLevel(draculaMovieId)).thenReturn(draculaCertificate);

        assertFalse(parentalControlService.isMovieWithinParentalControlLevel(parentalControlLevel, draculaMovieId));
    }

    @Test(expected = TitleNotFoundException.class)
    public void testMovieNotFound() throws Exception {
        String theGooniesMovieId = "tt0089218";
        String parentalControlLevel = "U";

        when(mockMovieService.getParentalControlLevel(theGooniesMovieId)).thenThrow(new TitleNotFoundException());

        parentalControlService.isMovieWithinParentalControlLevel(parentalControlLevel, theGooniesMovieId);
    }

    @Test(expected = TechnicalFailureException.class)
    public void testTechnicalFailure() throws Exception {
        String theGooniesMovieId = "tt0089218";
        String parentalControlLevel = "U";

        when(mockMovieService.getParentalControlLevel(theGooniesMovieId)).thenThrow(new TechnicalFailureException());

        parentalControlService.isMovieWithinParentalControlLevel(parentalControlLevel, theGooniesMovieId);
    }

    @Test(expected = CertificateNotFoundException.class)
    public void testInvalidParentalControl() throws Exception {
        String theGooniesCertificate = "PG";
        String theGooniesMovieId = "tt0089218";
        String parentalControlLevel = "";

        when(mockMovieService.getParentalControlLevel(theGooniesMovieId)).thenReturn(theGooniesCertificate);

        parentalControlService.isMovieWithinParentalControlLevel(parentalControlLevel, theGooniesMovieId);
    }

    @Test(expected = CertificateNotFoundException.class)
    public void testInvalidMovieCertificate() throws Exception {
        String theGooniesCertificate = "BL&*(";
        String theGooniesMovieId = "tt0089218";
        String parentalControlLevel = "PG";

        when(mockMovieService.getParentalControlLevel(theGooniesMovieId)).thenReturn(theGooniesCertificate);

        parentalControlService.isMovieWithinParentalControlLevel(parentalControlLevel, theGooniesMovieId);
    }
}