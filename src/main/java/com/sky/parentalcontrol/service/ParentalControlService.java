package com.sky.parentalcontrol.service;

import com.sky.movie.exceptions.CertificateNotFoundException;
import com.sky.movie.exceptions.TechnicalFailureException;
import com.sky.movie.exceptions.TitleNotFoundException;
import org.springframework.stereotype.Service;

/**
 * The Parental Control Service Interface.
 *
 * @author Roger Chick
 */
@Service
public interface ParentalControlService {

    /**
     * isMovieWithinParentalControlLevel takes the supplied parentalControlLevel and movieId as Strings, looks up the
     * movie certificate and compares that to the parentalControlLevel supplied.
     *
     * @param parentalControlLevel the parert
     * @param movieId              the unique id for a movie
     * @return It will return true if the movie is equal or below the parentalControlLevel.
     * @throws TitleNotFoundException       thrown if movie not found.
     * @throws TechnicalFailureException    thrown if a technical failure has occurred whilst calling the ParentalControlService.
     * @throws CertificateNotFoundException thrown if the supplied or retrieved certificate is invalid or unknown.
     */
    boolean isMovieWithinParentalControlLevel(String parentalControlLevel, String movieId) throws
            TitleNotFoundException,
            TechnicalFailureException,
            CertificateNotFoundException;
} 
