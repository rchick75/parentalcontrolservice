package com.sky.movie;

import com.sky.movie.exceptions.CertificateNotFoundException;

public enum Certificate {
    // The order of these is used to evaluate parental control levels,
    // if any new certificates are added please make sure they are added
    // in the appropriate order from least to most restrictive.
    U("U"), PG("PG"), TWELVE("12"), FIFTEEN("15"), EIGHTEEN("18");

    private String certificateText;

    Certificate(String certificateText) {
        this.certificateText = certificateText;
    }

    @Override
    public String toString() {
        return certificateText;
    }

    public static Certificate findByCertificateText(String cert) throws CertificateNotFoundException {
        if (cert != null || !cert.isEmpty()) {
            for (Certificate c : values()) {
                if (c.toString().equals(cert)) {
                    return c;
                }
            }
        }
        throw new CertificateNotFoundException(String.format("Certificate : '%s' is not found.", cert));
    }
}
