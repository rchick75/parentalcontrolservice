package com.sky.movie.exceptions;

/**
 *
 */
public class CertificateNotFoundException extends Exception {
    public CertificateNotFoundException(String message) {
        super(message);
    }
}
